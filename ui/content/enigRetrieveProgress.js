/*global Components: false*/
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/* eslint no-invalid-this: 0, no-loop-func: 0 */

"use strict";

const Cu = Components.utils;
const Ci = Components.interfaces;
const Cc = Components.classes;

Cu.import("chrome://enigmail/content/modules/core.jsm"); /*global EnigmailCore: false */
Cu.import("chrome://enigmail/content/modules/log.jsm"); /*global EnigmailLog: false */
Cu.import("chrome://enigmail/content/modules/locale.jsm"); /*global EnigmailLocale: false */
Cu.import("chrome://enigmail/content/modules/keyserver.jsm"); /*global EnigmailKeyServer: false */
Cu.import("chrome://enigmail/content/modules/errorHandling.jsm"); /*global EnigmailErrorHandling: false */
Cu.import("chrome://enigmail/content/modules/data.jsm"); /*global EnigmailData: false */
Cu.import("chrome://enigmail/content/modules/dialog.jsm"); /*global EnigmailDialog: false */
Cu.import("chrome://enigmail/content/modules/constants.jsm"); /*global EnigmailConstants: false */

// dialog is just an array we'll use to store various properties from the dialog document...
var gDialog;

// the msgProgress is a nsIMsgProgress object
var msgProgress = null;

// Progress Listener Object
var gProgressListener = {
  onCancel: function() {
    // onCancel will be overwritten by callee
  },

  onStart: function() {
    gDialog.progress.removeAttribute("value");
  },

  onStop: function() {
    // we are done transmitting
    // Indicate completion in status area.

    // Put progress meter at 100%.
    gDialog.progress.setAttribute("value", "100");

    window.close();
  },

  onProgress: function(percentage) {
    gDialog.progress.setAttribute("value", percentage);
  }
};


function onLoad() {
  // Set global variables.
  EnigmailLog.DEBUG("enigRetrieveProgress: onLoad\n");

  var inArg = window.arguments[0];
  window.arguments[1].result = false;

  gDialog = {};
  gDialog.strings = [];
  gDialog.progress = document.getElementById("dialog.progress");

  var enigmailSvc = EnigmailCore.getService(window);
  if (!enigmailSvc)
    return;

  msgProgress = Cc["@mozilla.org/messenger/progress;1"].createInstance(Ci.nsIMsgProgress);

  if (inArg.accessType == EnigmailConstants.UPLOAD_WKD) {
    uploadToWkd(inArg);
  }
  else {
    performKeyServerOperation(inArg);
  }

}

function uploadToWkd(inArg) {
  /*
  let statTxt = document.getElementById("dialog.status2");
  statTxt.value = EnigmailLocale.getString("keyserverTitle.uploading");
  document.getElementById("progressWindow").setAttribute("title", EnigmailLocale.getString("keyserverTitle.uploading"));

  let progressDlg = document.getElementById("dialog.progress");
  progressDlg.setAttribute("mode", "undetermined");

  msgProgress.processCanceledByUser = false;

  let observer = {
    get isCanceled() {
      return msgProgress.processCanceledByUser;
    },
    onProgress: function(completionRate) {
      progressDlg.setAttribute("value", completionRate);
      progressDlg.setAttribute("mode", "normal");
    },
    onUpload: function(fpr) {
      // do nothing
    },
    onFinished: function(completionStatus, errorMessage, displayError) {
      if (completionStatus !== 0) {
        window.close();
        gEnigCallbackFunc(completionStatus, errorMessage, displayError);
      }
      else {
        EnigmailDialog.info(window, EnigmailLocale.getString("keyserverProgress.wksUploadCompleted"));
        window.close();
      }
    }
  };

  EnigmailKeyServer.performWkdUpload(inArg, window, observer); */
}

function performKeyServerOperation(inArg) {
  EnigmailLog.DEBUG("enigRetrieveProgress.js: performKeyServerOperation\n");
  var subject;
  var statTxt = document.getElementById("dialog.status2");
  if (inArg.accessType == EnigmailConstants.UPLOAD_KEY || inArg.accessType == EnigmailConstants.UPLOAD_WKD) {
    statTxt.value = EnigmailLocale.getString("keyserverProgress.uploading");
    subject = EnigmailLocale.getString("keyserverTitle.uploading");
  }
  else {
    statTxt.value = EnigmailLocale.getString("keyserverProgress.refreshing");
    subject = EnigmailLocale.getString("keyserverTitle.refreshing");
  }

  let promise;
  switch (inArg.accessType) {
    case EnigmailConstants.DOWNLOAD_KEY:
      promise = EnigmailKeyServer.download(inArg.keyId.join(" "), inArg.keyServer, gProgressListener);
      break;
    case EnigmailConstants.UPLOAD_KEY:
      promise = EnigmailKeyServer.upload(inArg.keyId.join(" "), inArg.keyServer, gProgressListener);
      break;
    case EnigmailConstants.REFRESH_KEY:
      promise = EnigmailKeyServer.refresh(inArg.keyServer, gProgressListener);
      break;
  }

  promise.then(result => {
    processEnd(0, result);
  }).catch(errorMsg => {
    processEnd(1, errorMsg);
    window.close();
  });

  document.getElementById("progressWindow").setAttribute("title", subject);
}

function onUnload() {
  if (msgProgress) {
    try {
      msgProgress.unregisterListener(gProgressListener);
      msgProgress = null;
    } catch (exception) {}
  }
}

// If the user presses cancel, tell the app launcher and close the dialog...
function onCancel() {
  gProgressListener.onCancel();
  gProgressListener.onStop();
  return true;
}

function processEnd(resultStatus, details) {
  EnigmailLog.DEBUG(`enigmailRetrieveProgress.js: processEnd(): resultStatus=${resultStatus}
`);

  let returnObj = window.arguments[1];
  let inArg = window.arguments[0];

  let accessType = inArg.accessType;
  returnObj.exitCode = resultStatus;

  if (resultStatus === 0) {
    returnObj.result = true;

    switch (accessType) {
      case EnigmailConstants.DOWNLOAD_KEY:
      case EnigmailConstants.REFRESH_KEY:
        EnigmailDialog.info(window, EnigmailLocale.getString("keyserver.result.download", [details.keyList.length, inArg.keyId.length]));
        break;
      case EnigmailConstants.UPLOAD_KEY:
        EnigmailDialog.info(window, EnigmailLocale.getString("keyserver.result.upload", details.keyList.length));
    }
  }
  else {
    let message = "";
    switch (accessType) {
      case EnigmailConstants.DOWNLOAD_KEY:
      case EnigmailConstants.REFRESH_KEY:
        message = EnigmailLocale.getString("receiveKeysFailed");
        break;
      case EnigmailConstants.UPLOAD_KEY:
      case EnigmailConstants.UPLOAD_WKD:
        message = EnigmailLocale.getString("sendKeysFailed");
    }
    EnigmailDialog.alert(window, message + "\n" + details.errorDetails);
  }
  gProgressListener.onStop();
}
